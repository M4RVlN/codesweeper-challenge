﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public sealed class GameManager : MonoBehaviourSingleton<GameManager>
{
    public const int MeshScale = 4;
    public const float MapFeatureOffset = 20f;

    public static GameState GameState { get; private set; } = GameState.MainMenu;

    public Difficulty Difficulty { get; private set; }
    public GameMode GameMode { get; private set; }

    public Timer Timer { get; private set; }

    public int DiscoveredCells { get; private set; }
    public bool[,] Discovered { get; private set; }
    public byte[,] DangerLevel { get; private set; }

    public HashSet<Vec2> Mines { get; private set; }
    public Dictionary<Vec2, GameObject> Flags { get; private set; }

    public int Deaths { get; private set; }

    private Coroutine reaveling;

    #region Unity Fields
    public Transform Cursor;
    public GameObject BuojPrefab;
    public GameObject MinePrefab;
    public GameObject CellPrefab;
    public Material[] CellMaterials = new Material[9];

    //Map Features
    public List<GameObject> MapFeatures = new List<GameObject>();
    #endregion

    #region Button Controls
    public void NewGame()
    {
        UIManager.Instance.OpenUI(UIManager.NewGame);
        UIManager.Instance.HideHUD();
    }

    public void RestartGame()
        => StartNewGame(Difficulty, GameMode);

    public void StartNewGame(Difficulty diff, GameMode gm)
    {
        //Clear old object data
        ClearGame();

        //Create new data
        Init(diff, gm);

        UIManager.Instance.CloseUI();
        UIManager.Instance.ShowHUD();
    }

    public void ContinueGame()
    {
        //Clear old object data
        ClearGame();

        //Create new data
        Init(SaveGame.Load().Value);

        UIManager.Instance.CloseUI();
        UIManager.Instance.ShowHUD();
    }

    public void OpenPauseMenu()
    {
        GameState = GameState.PauseMenu;
        Timer.Stop();

        UIManager.Instance.OpenUI(UIManager.Pause);
    }

    public void ClosePauseMenu()
    {
        GameState = GameState.InGame;
        Timer.Start();

        UIManager.Instance.CloseUI();
    }

    public void CloseOptionsMenu()
    {
        if(GameState == GameState.MainMenu)
            UIManager.Instance.OpenUI(UIManager.Main);
        else if(GameState == GameState.PauseMenu)
            UIManager.Instance.OpenUI(UIManager.Pause);
    }

    public void OpenOptionsMenu()
    {
        UIManager.Instance.OpenUI(UIManager.Options);
    }

    public void QuitGame()
    {
        if (GameState == GameState.PauseMenu
            || GameState == GameState.InGame
            || GameState == GameState.GameOver)
        {
            SaveGame.Save();
        }

        ClearGame();

        //Return to main menu
        GameState = GameState.MainMenu;

        UIManager.Instance.OpenUI(UIManager.Main);
        UIManager.Instance.HideHUD();
    }

    public void ExitGame()
        => Application.Quit();
    #endregion

    #region Init
    private new void Awake()
    {
        ConfigManager.Init();
    }

    public void Init(Difficulty diff, GameMode gm)
    {
        Difficulty = diff;
        GameMode = gm;

        DangerLevel = new byte[Difficulty.GridSize, Difficulty.GridSize];
        Discovered = new bool[Difficulty.GridSize, Difficulty.GridSize];

        Mines = new HashSet<Vec2>();
        Flags = new Dictionary<Vec2, GameObject>();

        Deaths = 0;
        DiscoveredCells = 0;

        SpreadMines();
        SetDangerLevel();

        GenerateGridMesh(Difficulty.GridSize);
        CameraController.Instance.Init();
        SpawnMapFeatures();

        Timer = new Timer(diff.AvailableTime);
        Timer.Start();

        GameState = GameState.InGame;
        SaveGame.Delete();
    }

    public void Init(SaveGame save)
    {
        //Initialize
        Difficulty = new Difficulty(save.GridSize, save.Mines.Length, new TimeSpan(0, 0, save.AvailableTime));
        DangerLevel = new byte[save.GridSize, save.GridSize];
        Discovered = new bool[save.GridSize, save.GridSize];
        GameMode = save.GameMode;
        Deaths = save.Deaths;
        DiscoveredCells = 0;
        Mines = new HashSet<Vec2>();
        Flags = new Dictionary<Vec2, GameObject>();

        //Load Mines
        foreach (string s in save.Mines)
            Mines.Add(Vec2.FromString(s));

        //Load / Place Flags
        foreach (string s in save.Flags)
            PlaceFlag(Vec2.FromString(s), false);

        //Set Danger Level
        SetDangerLevel();

        //Init Discovered Cells / Place Mines
        for (int x = 0; x < save.GridSize; x++)
        {
            for (int y = 0; y < save.GridSize; y++)
            {
                if (save.Discovered[x * save.GridSize + y])
                {
                    var pos = new Vec2(x, y);

                    DiscoverCell(pos);

                    if (Mines.Contains(pos))
                        PlaceMine(pos, false);
                }
            }
        }

        GenerateGridMesh(Difficulty.GridSize);
        CameraController.Instance.Init();
        SpawnMapFeatures();

        Timer = new Timer(Difficulty.AvailableTime)
        {
            Offset = new TimeSpan(0, 0, save.ElapsedTime)
        };
        Timer.Start();

        GameState = GameState.InGame;
        SaveGame.Delete();
    }

    private void GenerateGridMesh(int size)
    {
        Mesh m = new Mesh();

        var verticies = new List<Vector3>();
        var normals = new List<Vector3>();
        var uvs = new List<Vector2>();

        for (int x = 0; x <= size; x++)
        {
            for (int y = 0; y <= size; y++)
            {
                verticies.Add(new Vector3(MeshScale * x, 0, MeshScale * y));
                normals.Add(Vector3.up);
                uvs.Add(new Vector2(x, y));
            }
        }

        var triangles = new List<int>();
        var vertCount = size + 1;
        for (int i = 0; i < vertCount * vertCount - vertCount; i++)
        {
            if ((i + 1) % vertCount == 0)
                continue;

            triangles.AddRange(new List<int>()
            {
                i + 1 + vertCount, i + vertCount, i,
                i, i + 1, i + vertCount + 1
            });
        }

        m.SetVertices(verticies);
        m.SetNormals(normals);
        m.SetUVs(0, uvs);
        m.SetTriangles(triangles, 0);

        GetComponent<MeshFilter>().mesh = m;
        GetComponent<MeshCollider>().sharedMesh = m;
    }

    private void SpreadMines()
    {
        int cells = Difficulty.GridSize * Difficulty.GridSize;
        int mineCount = Difficulty.MineCount;

        do
        {
            int rng = UnityEngine.Random.Range(0, cells - 1);
            int y = Mathf.FloorToInt(rng / Difficulty.GridSize);
            int x = rng - (y * Difficulty.GridSize);

            if (!Mines.Contains(new Vec2(x, y)))
            {
                Mines.Add(new Vec2(x, y));
                mineCount--;
            }
        }
        while (mineCount > 0);
    }

    private void SetDangerLevel()
    {
        foreach(Vec2 pos in Mines)
        {
            for(int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    Vec2 neighbor = new Vec2(pos.x + x, pos.y + y);
                    if (CheckBounds(neighbor))
                        DangerLevel[neighbor.x, neighbor.y]++;
                }
            }
        }
    }

    private void SpawnMapFeatures()
    {
        var gridSize = Difficulty.GridSize * MeshScale;
        
        for(int i = 0; i < 3; i++)
        {
            Vector3 pos = Vector3.zero;
            var feature = UnityEngine.Random.Range(0, MapFeatures.Count);

            switch (i)
            {
                case 0:
                    pos = new Vector3(-MapFeatureOffset, 0, UnityEngine.Random.Range(0, gridSize));
                    break;
                case 1:
                    pos = new Vector3(UnityEngine.Random.Range(0, gridSize), 0, gridSize + MapFeatureOffset);
                    break;
                case 2:
                    pos = new Vector3(gridSize + MapFeatureOffset, 0, UnityEngine.Random.Range(0, gridSize));
                    break;
            }

            Instantiate(MapFeatures[feature], pos, Quaternion.identity, this.transform);
        }
    }
    #endregion

    #region Logic
    public bool CheckBounds(Vec2 pos)
        => pos.x >= 0 && pos.x < Difficulty.GridSize && pos.y >= 0 && pos.y < Difficulty.GridSize;

    private IEnumerator RevealCells(Vec2 pos)
    {
        if (Flags.ContainsKey(pos))
        {
            reaveling = null;
            yield break;
        }

        if (Mines.Contains(pos) && !Discovered[pos.x, pos.y])
        {
            PlaceMine(pos);
            GameOver();
        }

        Queue<Vec2> currentQueue = new Queue<Vec2>();
        Queue<Vec2> nextQueue = new Queue<Vec2>();
        currentQueue.Enqueue(pos);

        do
        {
            while (currentQueue.Count > 0)
            {
                Vec2 current = currentQueue.Dequeue();
                if (!Flags.ContainsKey(current))
                    DiscoverCell(current);

                if (DangerLevel[current.x, current.y] == 0)
                {
                    for (int x = -1; x <= 1; x++)
                    {
                        for (int y = -1; y <= 1; y++)
                        {
                            Vec2 neighbor = new Vec2(current.x + x, current.y + y);

                            if (CheckBounds(neighbor))
                            {
                                if(!Discovered[neighbor.x, neighbor.y])
                                    nextQueue.Enqueue(neighbor);
                            }
                        }
                    }
                }
            }
            currentQueue = new Queue<Vec2>(nextQueue);
            nextQueue.Clear();

            yield return new WaitForSeconds(.1f);
        }
        while (currentQueue.Count > 0);

        //Check win condition
        if(DiscoveredCells == Difficulty.GridSize * Difficulty.GridSize - Mines.Count)
            Win();

        reaveling = null;
    }

    private void DiscoverCell(Vec2 pos)
    {
        if (CheckBounds(pos))
        {
            if (!Discovered[pos.x, pos.y])
            {
                Discovered[pos.x, pos.y] = true;

                //Return before adding to DiscoveredCells, because mines don't count
                if (Mines.Contains(pos))
                    return;

                DiscoveredCells++;

                //Create cell object
                var g = Instantiate(CellPrefab,
                                    new Vector3(pos.x * MeshScale, -0.01f, pos.y * MeshScale),
                                    Quaternion.identity,
                                    this.transform);
                g.GetComponent<MeshRenderer>().material = CellMaterials[DangerLevel[pos.x, pos.y]];
                g.name = $"Cell {pos.ToString()}";
            }
        }
    }

    private void PlaceFlag(Vec2 pos, bool freshSpawn = true)
    {
        if (Discovered[pos.x, pos.y])
            return;
        else if (Flags.ContainsKey(pos))
        {
            Destroy(Flags[pos]);
            Flags.Remove(pos);
        }
        else
        {
            var g = Instantiate(BuojPrefab,
                                new Vector3(pos.x * MeshScale + (MeshScale / 2f), -1, pos.y * MeshScale + (MeshScale / 2f)),
                                Quaternion.identity,
                                this.transform);

            if(freshSpawn)
                g.GetComponentInChildren<AudioSource>().Play();

            Flags.Add(pos, g);
        }
    }

    private void PlaceMine(Vec2 pos, bool freshSpawn = true)
    {
        var g = Instantiate(MinePrefab,
                            new Vector3(pos.x * MeshScale + (MeshScale / 2f), 0, pos.y * MeshScale + (MeshScale / 2f)),
                            Quaternion.identity,
                            this.transform);
        if (freshSpawn)
        {
            g.GetComponentInChildren<ParticleSystem>().Play();
            g.GetComponentInChildren<AudioSource>().Play();
        }
    }

    private void Win()
    {
        UIManager.Instance.OpenUI("Win Screen");
        GameState = GameState.Win;
        Timer.Stop();
    }

    private void GameOver()
    {
        UIManager.Instance.OpenUI("GameOver Screen");
        GameState = GameState.GameOver;
        Deaths++;
        Timer.Stop();
    }

    private void ClearGame()
    {
        for (int i = 0; i < this.transform.childCount; i++)
            Destroy(this.transform.GetChild(i).gameObject);

        GetComponent<MeshFilter>().mesh = null;
        GetComponent<MeshCollider>().sharedMesh = null;
    }

    private void Update()
    {
        //Ingame Logic
        if(GameState == GameState.InGame)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                && !EventSystem.current.IsPointerOverGameObject())
            {
                Vec2 cursorPos = new Vec2(Mathf.FloorToInt(hit.point.x / MeshScale),
                                            Mathf.FloorToInt(hit.point.z / MeshScale));

                Cursor.gameObject.SetActive(true);
                Cursor.position = new Vector3(cursorPos.x * MeshScale, 0, cursorPos.y * MeshScale);

                if (Input.GetMouseButtonUp(0) && reaveling == null)
                {
                    reaveling = StartCoroutine(RevealCells(cursorPos));
                }
                else if (Input.GetMouseButtonUp(1))
                {
                    PlaceFlag(cursorPos);
                }
            }
            else
                Cursor.gameObject.SetActive(false);
        }
        else
            Cursor.gameObject.SetActive(false);

        //Pause Menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameState == GameState.InGame)
                OpenPauseMenu();
            else if(GameState == GameState.PauseMenu)
                ClosePauseMenu();
        }

        //Game Over when out of time
        if(GameMode == GameMode.Timed && Timer.Remaining.Ticks <= 0 && Deaths == 0)
            GameOver();
    }

    private void OnApplicationQuit()
    {
        if (GameState == GameState.PauseMenu
            || GameState == GameState.InGame
            || GameState == GameState.GameOver)
        {
            SaveGame.Save();
        }
    }
    #endregion
}