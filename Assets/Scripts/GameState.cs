﻿using UnityEngine;

public enum GameState
{
    MainMenu,
	PauseMenu,
    InGame,
    GameOver,
    Win
}
