﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class NewGameManager : MonoBehaviour
{
    #region Unity Fields
    public Dropdown gameModeDropdown;
    public Dropdown difficultyDropdown;
    public Toggle moreMinesToggle;
    public InputField minesInputField;
    public InputField fieldSizeInputField;
    public InputField timeHoursInputField;
    public InputField timeMinutesInputField;
    public InputField timeSecondsInputField;

    public Text fieldAmount;
    #endregion

    private void Start()
    {
        UpdateSettingValues();
    }

    private void OnGUI()
    {
        var i = TryParseInt(fieldSizeInputField.text);
        fieldAmount.text = (i * i).ToString() + " Fields";
    }

    private Difficulty GetDifficulty()
    {
        switch (difficultyDropdown.value)
        {
            case 0:
                return moreMinesToggle.isOn ? Difficulty.BeginnerHard : Difficulty.BeginnerEasy;

            case 1:
                return moreMinesToggle.isOn ? Difficulty.AdvancedHard : Difficulty.AdvancedEasy;

            case 2:
                return moreMinesToggle.isOn ? Difficulty.ProHard : Difficulty.ProEasy;

            case 3:
                var time = gameModeDropdown.value == (int)GameMode.Timed
                            ? new TimeSpan(TryParseInt(timeHoursInputField.text),
                                           TryParseInt(timeMinutesInputField.text),
                                           TryParseInt(timeSecondsInputField.text))
                            : new TimeSpan();

                return new Difficulty(TryParseInt(fieldSizeInputField.text),
                                      TryParseInt(minesInputField.text),
                                      time);

            default:
                throw new ArgumentOutOfRangeException("difficultyDropdown.value");
        }
    }

    private int TryParseInt(string value)
        => string.IsNullOrEmpty(value) ? 0 : int.Parse(value);

    public void UpdateSettingValues()
    {
        var difficulty = GetDifficulty();
        bool timed = gameModeDropdown.value == (int)GameMode.Timed;
        bool custom = difficultyDropdown.value == 3;

        moreMinesToggle.interactable = !custom;
        minesInputField.interactable = custom;
        fieldSizeInputField.interactable = custom;
        timeHoursInputField.interactable = custom && timed;
        timeMinutesInputField.interactable = custom && timed;
        timeSecondsInputField.interactable = custom && timed;

        minesInputField.text = difficulty.MineCount.ToString();
        fieldSizeInputField.text = difficulty.GridSize.ToString();
        timeHoursInputField.text = difficulty.AvailableTime.Hours.ToString();
        timeMinutesInputField.text = difficulty.AvailableTime.Minutes.ToString();
        timeSecondsInputField.text = difficulty.AvailableTime.Seconds.ToString();
    }

    public void StartGame()
        => GameManager.Instance.StartNewGame(GetDifficulty(), (GameMode)gameModeDropdown.value);
}