﻿using UnityEngine;

public class UIManager : MonoBehaviourSingleton<UIManager>
{
    public const string Main = "Main Menu";
    public const string Pause = "Pause Menu";
    public const string Options = "Options Menu";
    public const string Win = "Win Screen";
    public const string GameOver = "GameOver Screen";
    public const string NewGame = "New Game Menu";

    #region Unity Fields
    public GameObject Hud;
    public GameObject[] Menus;
    #endregion

    private void Start()
    {
        HideHUD();
        OpenUI(Main);
    }

    public void ShowHUD()
        => Hud.SetActive(true);

    public void HideHUD()
        => Hud.SetActive(false);

    public void OpenUI(string name)
    {
        foreach (GameObject g in Menus)
            g.SetActive(g.name == name);
    }

    public void CloseUI()
    {
        foreach (GameObject g in Menus)
            g.SetActive(false);
    }
}