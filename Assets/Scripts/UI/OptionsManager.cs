﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class OptionsManager : MonoBehaviourSingleton<OptionsManager>
{
    public static List<Resolution> Resolutions => new List<Resolution>()
    {
        new Resolution(){ width = 800, height = 600},
        new Resolution(){ width = 1024, height = 600},
        new Resolution(){ width = 1024, height = 768},
        new Resolution(){ width = 1152, height = 864},
        new Resolution(){ width = 1280, height = 720},
        new Resolution(){ width = 1280, height = 768},
        new Resolution(){ width = 1280, height = 800},
        new Resolution(){ width = 1280, height = 1024},
        new Resolution(){ width = 1360, height = 768},
        new Resolution(){ width = 1366, height = 768},
        new Resolution(){ width = 1440, height = 900},
        new Resolution(){ width = 1536, height = 864},
        new Resolution(){ width = 1600, height = 900},
        new Resolution(){ width = 1680, height = 1050},
        new Resolution(){ width = 1920, height = 1080},
        new Resolution(){ width = 1920, height = 1200},
        new Resolution(){ width = 2560, height = 1080},
        new Resolution(){ width = 2560, height = 1440},
        new Resolution(){ width = 3440, height = 1440},
        new Resolution(){ width = 3840, height = 2160},
    };

    #region Unity Fields
    public GameObject MainBackground;
    public GameObject PauseBackground;

    public Dropdown ResolutionDropdown;
    public Toggle FullscreenToggle;
    public Slider VolumeSlider;
    public Text VolumeText;
    public Dropdown LanguageDropdown;
    #endregion

    private void OnEnable()
    {
        //Add Resolutions
        var options = new List<string>();
        var findRes = FindResolution();
        
        ResolutionDropdown.ClearOptions();

        foreach (Resolution r in Resolutions)
            options.Add($"{r.width}x{r.height}");
        ResolutionDropdown.AddOptions(options);

        if (findRes.HasValue)
            ResolutionDropdown.value = findRes.Value;
        else
        {
            ResolutionDropdown.AddOptions(new List<string>() { "Unknown" });
            ResolutionDropdown.value = ResolutionDropdown.options.Count - 1;
        }

        //Add Languages
        options = new List<string>();

        LanguageDropdown.ClearOptions();

        foreach (LanguageData l in LocalizationManager.TranslationFiles)
            options.Add($"{l.LocalizedName}({l.UnlocalizedName})");
        LanguageDropdown.AddOptions(options);
        LanguageDropdown.value = LocalizationManager.CurrentLanguage;

        MainBackground.SetActive(GameManager.GameState == GameState.MainMenu);
        PauseBackground.SetActive(GameManager.GameState == GameState.PauseMenu);
        FullscreenToggle.isOn = Screen.fullScreen;
        VolumeText.text = AudioListener.volume.ToString("0.##");
    }

    private static int? FindResolution()
    {
        var cRes = Screen.currentResolution;

        for (int i = 0; i < Resolutions.Count; i++)
        {
            if (cRes.width == Resolutions[i].width && cRes.height == Resolutions[i].height)
                return i;
        }
        return null;
    }

    public void ChangeVolume()
    {
        AudioListener.volume = VolumeSlider.value;
        VolumeText.text = VolumeSlider.value.ToString("0.##");
    }

    public void ChangeLanguage()
    {
        LocalizationManager.LoadLanguage(LanguageDropdown.value);
    }

	public void SaveChanges()
    {
        Resolution res;
        if (FindResolution().HasValue)
        {
            res = Resolutions[ResolutionDropdown.value];
            Screen.SetResolution(res.width, res.height, FullscreenToggle.isOn);
        }
        else
        {
            res = Screen.currentResolution;
        }

        ConfigManager.SetConfig(new ConfigObject()
        {
            fullscreen = FullscreenToggle.isOn,
            language = LocalizationManager.GetLanguageName(LanguageDropdown.value),
            resolution = new int[2] { res.width, res.height },
            volume = AudioListener.volume
        });
    }
}
