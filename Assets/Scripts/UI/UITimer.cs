﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UITimer : MonoBehaviour
{
    private void OnGUI()
    {
        if (GameManager.GameState != GameState.MainMenu)
        {
            if (GameManager.Instance.GameMode == GameMode.Timed)
                GetComponent<Text>().text = GameManager.Instance.Timer.CountDownToString();
            else
                GetComponent<Text>().text = GameManager.Instance.Timer.ToString();
        }
    }
}
