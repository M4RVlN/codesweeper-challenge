﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIDeathCounter : MonoBehaviour
{
    private void OnGUI()
    {
        if (GameManager.GameState != GameState.MainMenu)
            GetComponent<Text>().text = GameManager.Instance.Deaths.ToString();
    }
}
