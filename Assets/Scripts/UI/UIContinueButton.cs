﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIContinueButton : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<Button>().interactable = SaveGame.FileExists();
    }
}
