﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIMineCounter : MonoBehaviour
{
    private void OnGUI()
    {
        if (GameManager.GameState != GameState.MainMenu)
            GetComponent<Text>().text = (GameManager.Instance.Mines.Count - GameManager.Instance.Flags.Count - GameManager.Instance.Deaths).ToString();
    }
}
