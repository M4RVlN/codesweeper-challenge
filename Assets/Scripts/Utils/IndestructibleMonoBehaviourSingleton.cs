﻿using UnityEngine;

public abstract class IndestructibleMonoBehaviourSingleton<T> : MonoBehaviour
    where T : IndestructibleMonoBehaviourSingleton<T>
{
    private static bool singletonHasBeenDestroyed;

    private static T _instance;
    public static T Instance
    {
        get
        {
            if (singletonHasBeenDestroyed)
            {
                return null;
            }
            if (!_instance)
            {
                Object[] array = FindObjectsOfType(typeof(T));
                if (array.Length > 0)
                {
                    if (array.Length > 1)
                    {
                    }
                    _instance = (T)(array[0]);
                }
                else
                {
                    GameObject gameObject = new GameObject(typeof(T).Name);
                    _instance = gameObject.AddComponent<T>();
                }
                DontDestroyOnLoad(_instance.transform.root);
            }
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
        }
        else
        {
            _instance = (T)this;
            if (transform.root == transform)
            {
                DontDestroyOnLoad(_instance);
            }
        }
    }

    protected virtual void OnDestroy()
    {
        if (_instance == this)
        {
            singletonHasBeenDestroyed = true;
        }
    }
}
