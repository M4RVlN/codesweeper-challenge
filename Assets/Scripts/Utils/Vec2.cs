﻿using System.Text.RegularExpressions;

public struct Vec2
{
    public int x { get; set; }
    public int y { get; set; }

	public Vec2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static bool operator ==(Vec2 a, Vec2 b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Vec2 a, Vec2 b)
    {
        return a.x != b.x || a.y != b.y;
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 17;
            hash = hash * 23 + x.GetHashCode();
            hash = hash * 23 + y.GetHashCode();
            return hash;
        }
    }

    public override string ToString()
    {
        return $"({x}, {y})";
    }

    public static Vec2 FromString(string s)
    {
        Regex r = new Regex(@"\d+");
        MatchCollection c = r.Matches(s);
        try
        {
            return new Vec2(int.Parse(c[0].Value), int.Parse(c[1].Value));
        }
        catch
        {
            return new Vec2(0, 0);
        }
    }
}
