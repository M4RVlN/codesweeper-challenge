﻿using System;
using System.Diagnostics;

public class Timer : Stopwatch
{
    private TimeSpan runtime;
    public TimeSpan Offset { get; set; }

    public TimeSpan RealElapsed
    {
        get
        {
            return Elapsed.Add(Offset);
        }
    }

    public TimeSpan Remaining
    {
        get
        {
            return runtime.Subtract(RealElapsed);
        }
    }

    public Timer(TimeSpan runtime = new TimeSpan())
    {
        this.runtime = runtime;
    }

    public string CountDownToString()
    => Remaining.Hours > 0
        ? Remaining.ToString(@"hh\:mm\:ss")
        : Remaining.ToString(@"mm\:ss");

    public override string ToString()
    => RealElapsed.Hours > 0
        ? RealElapsed.ToString(@"hh\:mm\:ss")
        : RealElapsed.ToString(@"mm\:ss");
}