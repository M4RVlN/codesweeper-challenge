﻿using UnityEngine;

public abstract class MonoBehaviourSingleton<T> : MonoBehaviour
    where T : MonoBehaviourSingleton<T>
{
    private static T instance;

    private static bool singletonHasBeenDestroyed;

    public static T Instance
    {
        get
        {
            if (singletonHasBeenDestroyed)
                return null;

            if (!instance)
            {
                Object[] array = FindObjectsOfType(typeof(T));

                if (array.Length > 0)
                    instance = (T)(array[0]);
                else
                {
                    GameObject gameObject = new GameObject(typeof(T).Name);
                    instance = gameObject.AddComponent<T>();
                }
            }
            return instance;
        }
    }

    protected virtual void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this);
        else
            instance = (T)this;
    }

    protected virtual void OnDestroy()
    {
        if (instance == this)
            singletonHasBeenDestroyed = true;
    }
}
