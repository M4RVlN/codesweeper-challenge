﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioBackgroundMusic : MonoBehaviour
{
    private void Update()
    {
        if (GameManager.GameState == GameState.MainMenu)
            GetComponent<AudioSource>().volume = 1f;
        else
            GetComponent<AudioSource>().volume = 0.5f;
    }
}
