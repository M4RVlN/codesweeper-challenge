﻿using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public static class ConfigManager
{
    public static string FilePath => Path.Combine(Application.streamingAssetsPath, "config.json");

    public static ConfigObject Config { get; set; }

    internal static void Init()
    {
        if (!File.Exists(FilePath))
        {
            Config = new ConfigObject()
            {
                resolution = new int[2] { Screen.currentResolution.width, Screen.currentResolution.height },
                fullscreen = Screen.fullScreen,
                volume = 1,
                language = "English"
            };
            File.WriteAllText(FilePath, JsonConvert.SerializeObject(Config));
        }
        else
        {
            Config = JsonConvert.DeserializeObject<ConfigObject>(File.ReadAllText(FilePath));
        }

        Screen.SetResolution(Config.resolution[0], Config.resolution[1], Config.fullscreen);
        AudioListener.volume = Config.volume;
        LocalizationManager.LoadLanguage(Config.language);
    }

    public static void SetConfig(ConfigObject c)
    {
        Config = c;
        File.WriteAllText(FilePath, JsonConvert.SerializeObject(Config));
    }
}

[System.Serializable]
public sealed class ConfigObject
{
    public int[] resolution;
    public bool fullscreen;
    public float volume;
    public string language;
}