﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

//TODO use Newtonsoft.Json
[Serializable]
public struct SaveGame
{
    public static string FilePath => Path.Combine(Application.streamingAssetsPath, "quit.sav");

    public int GridSize;
    public int AvailableTime;
    public int ElapsedTime;
    public GameMode GameMode;
    public bool[] Discovered;
    public string[] Mines;
    public string[] Flags;
    public int Deaths;

    public static bool FileExists()
        => File.Exists(FilePath);

    public static void Save()
    {
        if (GameManager.Instance == null)
            return;

        //Copy Data to SaveGame
        var save = new SaveGame()
        {
            GridSize = GameManager.Instance.Difficulty.GridSize,
            GameMode = GameManager.Instance.GameMode,

            AvailableTime = (int)GameManager.Instance.Difficulty.AvailableTime.TotalSeconds,

            ElapsedTime = (int)GameManager.Instance.Timer.RealElapsed.TotalSeconds,
            Deaths = GameManager.Instance.Deaths
        };

        //Copy Discoverd Cells
        save.Discovered = new bool[GameManager.Instance.Discovered.Length];
        Buffer.BlockCopy(GameManager.Instance.Discovered, 0, save.Discovered, 0, GameManager.Instance.Discovered.Length);

        //Copy Mines
        save.Mines = new string[GameManager.Instance.Mines.Count];
        int i = 0;
        foreach (Vec2 v in GameManager.Instance.Mines)
        {
            save.Mines[i] = v.ToString();
            i++;
        }

        //Copy Flags
        save.Flags = new string[GameManager.Instance.Flags.Count];
        i = 0;
        foreach(Vec2 v in GameManager.Instance.Flags.Keys)
        {
            save.Flags[i] = v.ToString();
            i++;
        }

        //Convert to Json, then to binary
        string jsonData = JsonUtility.ToJson(save);
        byte[] jsonByte = Encoding.ASCII.GetBytes(jsonData);

        //Write to File
        try
        {
            File.WriteAllBytes(FilePath, jsonByte);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed to create save file: " + e.Message);
        }
    }

    public static SaveGame? Load()
    {
        if (!File.Exists(FilePath))
            return null;

        //Load saved Json
        byte[] jsonByte = null;
        try
        {
            jsonByte = File.ReadAllBytes(FilePath);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed to load save file: " + e.Message);
            return null;
        }

        //Convert to Json, then to SaveGame
        string jsonData = Encoding.ASCII.GetString(jsonByte);
        return JsonUtility.FromJson<SaveGame>(jsonData);
    }

    public static void Delete()
    {
        try
        {
            File.Delete(FilePath);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed to delete save file: " + e.Message);
        }
    }
}
