﻿using UnityEngine;

public class CameraController : MonoBehaviourSingleton<CameraController>
{
    private Vector3 cameraVelocity;
    private float boardSize;

    //Unity Field
    public float keyboardScrollSpeed;
    public float keyboardDrag;

    internal void Init()
    {
        boardSize = GameManager.Instance.Difficulty.GridSize * GameManager.MeshScale;

        transform.position = new Vector3(GameManager.Instance.Difficulty.GridSize * GameManager.MeshScale / 2f,
                                         20,
                                         GameManager.Instance.Difficulty.GridSize * GameManager.MeshScale / 2f - 20);
    }

    private void Update()
    {
        Vector3 velocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //Smooth camera movement
        cameraVelocity = Vector3.Lerp(cameraVelocity,
                                      velocity * keyboardScrollSpeed,
                                      Time.deltaTime * keyboardDrag);

        //Keep camera in bounds
        if (transform.position.x <= 0 && cameraVelocity.x < 0)
            cameraVelocity.x = 0;
        if (transform.position.x >= boardSize && cameraVelocity.x > 0)
            cameraVelocity.x = 0;
        if (transform.position.z <= -10 && cameraVelocity.z < 0)
            cameraVelocity.z = 0;
        if (transform.position.z >= boardSize - 10 && cameraVelocity.z > 0)
            cameraVelocity.z = 0;

        //Apply Velocity
        transform.position += cameraVelocity;
    }
}