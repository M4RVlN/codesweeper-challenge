﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public sealed class LocalizedText : MonoBehaviour
{
    #region Unity Fields
    public string NameTag;
    #endregion

    private void OnGUI()
    {
        GetComponent<Text>().text = LocalizationManager.GetLocalization(NameTag);
    }
}