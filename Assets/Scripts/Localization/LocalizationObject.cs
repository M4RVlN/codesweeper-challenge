﻿using System.Collections.Generic;

[System.Serializable]
public class LocalizationObject
{
    public string localizedName;
    public string unlocalizedName;
    public Dictionary<string,string> items;
}