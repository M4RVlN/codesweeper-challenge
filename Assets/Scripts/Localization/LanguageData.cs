﻿[System.Serializable]
public struct LanguageData
{
    public string LocalizedName { get; set; }
    public string UnlocalizedName { get; set; }
    public string Path { get; set; }
}