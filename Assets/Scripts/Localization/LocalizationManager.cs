﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System;
using System.Text;

public class LocalizationManager : IndestructibleMonoBehaviourSingleton<LocalizationManager>
{
    public static List<LanguageData> TranslationFiles = new List<LanguageData>();

    public static Dictionary<string, string> LocalizedText;

    public static int CurrentLanguage;

    private new void Awake()
    {
        var paths = Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "Localization"), "*.json");

        foreach (string p in paths)
        {
            var data = GetLanguageObject(p);
            if (data != null)
            {
                TranslationFiles.Add(new LanguageData()
                {
                    LocalizedName = data.localizedName,
                    UnlocalizedName = data.unlocalizedName,
                    Path = p
                });
            }
        }

        LoadLanguage(ConfigManager.Config.language);
    }

    public static string GetLanguageName(int i)
        => TranslationFiles[i].UnlocalizedName;

    public static LocalizationObject GetLanguageObject(string filePath)
    {
        try
        {
            string jsonData = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<LocalizationObject>(jsonData);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            return null;
        }
    }

    public static void LoadLanguage(string langName)
    {
        for (int i = 0; i < TranslationFiles.Count; i++)
        {
            if(TranslationFiles[i].UnlocalizedName == langName)
            {
                LoadLanguage(i);
                return;
            }
        }
    }

    public static void LoadLanguage(int fileNumber)
    {
        CurrentLanguage = fileNumber;
        LocalizationObject data = GetLanguageObject(TranslationFiles[fileNumber].Path);

        if (data != null)
            LocalizedText = new Dictionary<string, string>(data.items);
    }

    public static string GetLocalization(string key)
        => LocalizedText?.ContainsKey(key) ?? false ? LocalizedText[key] : "Missing Translation";
}
