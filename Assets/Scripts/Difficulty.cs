﻿using System;

public struct Difficulty
{
    public static Difficulty BeginnerEasy { get; } = new Difficulty(9, 10, new TimeSpan(0, 3, 0));
    public static Difficulty BeginnerHard { get; } = new Difficulty(9, 35, new TimeSpan(0, 3, 0));
    public static Difficulty AdvancedEasy { get; } = new Difficulty(16, 40, new TimeSpan(0, 8, 0));
    public static Difficulty AdvancedHard { get; } = new Difficulty(16, 99, new TimeSpan(0, 3, 0));
    public static Difficulty ProEasy { get; } = new Difficulty(22, 99, new TimeSpan(0, 24, 0));
    public static Difficulty ProHard { get; } = new Difficulty(22, 170, new TimeSpan(0, 3, 0));

    public int GridSize { get; }
    public int MineCount { get; }
    public TimeSpan AvailableTime { get; }

    public Difficulty(int size, int mines, TimeSpan time = new TimeSpan())
    {
        GridSize = size;
        MineCount = mines;
        AvailableTime = time;
    }
}